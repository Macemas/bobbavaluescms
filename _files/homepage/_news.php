<?php
if ($GET[2] == "") {
    header('Location: ' . $_SITE['path'] . '/index');
    exit;
} else {
    $query = $mysqli->query("SELECT * FROM news WHERE id = '" . $GET[1] . "' AND title = '" . $GET[2] . "'");
    $row = $query->fetch_object();
    $allnews = $mysqli->query("SELECT * FROM news ORDER BY id DESC");
    $comments_query = $mysqli->query("SELECT * FROM news_kommentare WHERE news_id = '" . $GET[1] . "'");
    //Kommentieren
    if (isset($_POST['comment'])) {
        if (empty($_POST['text'])) {
            header('Location:' . $_SITE['path'] . '/index');
            exit;
        } else {
            $mysqli->query("INSERT INTO news_kommentare SET news_id = '" . $GET[1] . "', user_id = '" . $myrow->id . "', text = '" . $_POST['text'] . "'");
        }
    }
    ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-3"> 
                <div id="box" class="news_list">
                    <?php while ($news = $allnews->fetch_object()) { ?>
                        <a style="color:black;" href="<?php echo $_SITE['path'] ?>/news/<?php echo $news->id ?>/<?php echo $news->title ?>"><div class="news_select <?php
                            if ($GET[1] == $news->id) {
                                echo 'select';
                            }
                            ?>"><?php echo $news->title ?></div></a>
                        <?php } ?>
                </div>
            </div>

            <div class="col-sm-12 col-md-9">  
                <div id="box"  class="news_body">
                    <h4><?php echo $row->title ?> <div style="float:right;"><?php echo date('d.m.Y', $row->timestamp); ?></div></h4>
                    <?php echo $row->content ?>
                </div>
                <div id="box"  class="news_author">
                    Dieser Artikel wurde verfasst von: <b><?php echo $row->author ?></b>
                </div>
                <?php if ($myrow->rank > 0) { ?>
                    <form action="" method="POST">
                        <textarea name="text"  class="news_comment_box" placeholder="Kommentar schreiben"></textarea>
                        <button type="submit" name="comment">Absenden</button>
                    <?php } ?>
                </form>
                <?php
                while ($comments = $comments_query->fetch_object()) {
                    $query = $mysqli->query("SELECT id, username FROM users WHERE id = '" . $comments->user_id . "'");
                    $row = $query->fetch_object();
                    ?>
                    <div id="box" class="comment_user" style="background:url(http://www.habbost.us/habbo-imaging/avatarimage.php?username=<?php echo $row->username ?>&gesture=sml&img_format=gif) 10px 50% no-repeat;background-color:white;">
                        <b><?php echo $row->username ?></b>: <?php echo $comments->text ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>




    </div>
<?php } ?>