<?php
//News
$news_query = $mysqli->query("SELECT id, title, img FROM news ORDER BY id DESC LIMIT 4");
//Letzen Änderungen
$rare_query = $mysqli->query("SELECT furni_id, name, price FROM prices ORDER BY last_timestamp DESC LIMIT 12");
?>
<div class="container">    
	    <div class="row">
        <div class="col-sm-12 col-md-12">  <div id="title" style="margin-top:10px;">Latest Rare changes</div> </div>
        <?php while ($rare = $rare_query->fetch_object()) { ?>
            <div class="col-sm-12 col-md-3">
                <div id="box" class="rare_box">
                    <div style="background:url(<?php echo $_SITE['path'] ?>/public/img/rare/<?php echo $rare->furni_id ?>.gif)50% 80% no-repeat;height:100%;width:100%;">
                    <div id="title"> <?php echo $rare->name ?> </div>
                    <div class="preis"><?php echo number_format($rare->price, 0, ' ', ' '); ?></div> 
                   
                    </div>
                </div>
                <a href="<?php echo $_SITE['path'] ?>/rare/<?php echo $rare->furni_id ?>/<?php echo $rare->name ?>"><button>More Info</button></a>
            </div>
        <?php } ?>

        <div class="col-sm-12 col-md-12">  <div id="title">Latest articles</div> </div>
        <?php while ($news = $news_query->fetch_object()) { ?>
            <div class="col-sm-12 col-md-3">
                <div id="box" class="news_index" style="background:url(<?php echo $news->img ?>) 50% 50%;">
                    <div id="title"> <?php echo $news->title ?> </div>
                </div>
                <a href="<?php echo $_SITE['path'] ?>/news/<?php echo $news->id ?>/<?php echo $news->title ?>"><button>Read</button></a>
            </div>
        <?php } ?>
    </div>
    </div>
</div>
