<?php
//Rares
$rare_query = $mysqli->query("SELECT * FROM prices WHERE furni_id = '" . $GET[1] . "'");
$row = $rare_query->fetch_object();
$category_query = $mysqli->query("SELECT * FROM categories WHERE id = '" . $row->category . "'");
$category = $category_query->fetch_object();
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-3">
            <div id="box" class="news_index" style="background:url(<?php echo $_SITE['path'] ?>/public/img/itembg.png) 50% 50%;height:210px;">
                <div style="background:url(<?php echo $_SITE['path'] ?>/public/img/rare/<?php echo $row->furni_id ?>.gif) 50% 50% no-repeat;height:210px;" id="inner">
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-9">
            <div id="box" class="leiste"><img src="<?php echo $_SITE['path'] ?>/public/img/icon_name.gif">Name: <b><?php echo $row->name ?></b></div>
        </div>
        <div class="col-sm-12 col-md-9">
            <div id="box" class="leiste two"><img src="<?php echo $_SITE['path'] ?>/public/img/icon_cat.gif"> Category: <b><a style="color:black;" href="<?php echo $_SITE['path'] ?>/kategorie/<?php echo $category->id ?>/<?php echo $category->name ?>"><?php echo $category->name ?></a></b></div>
        </div>
        <div class="col-sm-12 col-md-9">
            <div id="box" class="leiste"><img src="<?php echo $_SITE['path'] ?>/public/img/icon_price_alt.gif"> Value: <b><?php echo $row->price ?></b></div>
        </div>
        <div class="col-sm-12 col-md-9">
            <div id="box" class="leiste two"><img src="<?php echo $_SITE['path'] ?>/public/img/icon_price_alt.gif"> Previous Value: <b><?php echo $row->last_price ?></b></div>
        </div>
        <div class="col-sm-12 col-md-9">
            <div id="box" class="leiste"><img style="margin-right:15px;" src="<?php echo $_SITE['path'] ?>/public/img/icon_author_alt.gif"> Last modified by: <b><?php echo $row->last_author ?></b></div>
        </div>
        <div class="col-sm-12 col-md-9">
            <div id="box" class="leiste two"><img src="<?php echo $_SITE['path'] ?>/public/img/rad.gif"> Last modified date:  <b><?php echo date('d.m.Y', $row->last_timestamp) ?></b></div>
        </div>
        <div class="col-sm-12 col-md-12">
            <?php if ($myrow->rank > 1) { ?><a href="<?php echo $_SITE['path'] ?>/acp/rare/edit/<?php echo $row->furni_id ?>"><button style="margin-top:5px;">Edit Rare</button></a> <?php } ?>
        </div>
    </div>
</div>