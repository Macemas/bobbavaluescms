<?php
//Alle Kategorien
if ($GET[1] == 'all') {
    $query = $mysqli->query("SELECT * FROM categories ORDER BY id DESC");
    ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">  <div id="title">All categories</div> </div>
            <?php
            while ($row = $query->fetch_object()) {
                $rare_query = $mysqli->query("SELECT * FROM prices WHERE category = '" . $row->id . "' ORDER BY rand()");
                $rare = $rare_query->fetch_object();
                ?>
                <div class="col-sm-12 col-md-3">
                    <div id="box" class="rare_box">
                        <div style="background:url(<?php echo $_SITE['path'] ?>/public/img/rare/<?php echo $rare->furni_id ?>.gif)50% 80% no-repeat;height:100%;width:100%;">
                            <div id="title"> <?php echo $row->name ?> </div>

                        </div>
                    </div>
                    <a href="<?php echo $_SITE['path'] ?>/category/<?php echo $row->id ?>/<?php echo $row->name ?>"><button>More Info</button></a>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } else { ?>

    <?php
    $query = $mysqli->query("SELECT * FROM prices WHERE category = '" . $GET[1] . "'");
    $kategorie = $mysqli->query("SELECT name FROM categories WHERE id = '" . $GET[1] . "'");
    $categoy = $kategorie->fetch_object();
    ?>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">  <div id="title"><?php echo $categoy->name ?></div> </div>
            <?php
            while ($row = $query->fetch_object()) {
                ?>
                <div class="col-sm-12 col-md-3">
                    <div id="box" class="rare_box">
                        <div style="background:url(<?php echo $_SITE['path'] ?>/public/img/rare/<?php echo $row->furni_id ?>.gif)50% 80% no-repeat;height:100%;width:100%;">
                            <div id="title"> <?php echo $row->name ?> </div>
                             <div class="preis"><?php echo number_format($row->price, 0, ' ', ' '); ?></div> 
                        </div>
                    </div>
                    <a href="<?php echo $_SITE['path'] ?>/rare/<?php echo $row->furni_id ?>/<?php echo $row->name ?>"><button>More Info</button></a>
                </div>
            <?php } ?>
        </div>
    </div>


<?php } ?>