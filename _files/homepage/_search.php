<?php
$query = $mysqli->query("SELECT * FROM prices WHERE name LIKE '%" . $GET[1] . "%'");
?>
<div class="container">
    <div class="row">
        <?php while ($row = $query->fetch_object()) { ?>

            <div class="col-sm-12 col-md-3">
                <div id="box" class="rare_box">
                    <div style="background:url(<?php echo $_SITE['path'] ?>/public/img/rare/<?php echo $row->furni_id ?>.gif)50% 80% no-repeat;height:100%;width:100%;">
                        <div id="title"> <?php echo $row->name ?> </div>
                        <div class="preis"><?php echo number_format($row->price, 0, ' ', ' '); ?></div> 
                    </div>
                </div>
                <a href="<?php echo $_SITE['path'] ?>/rare/<?php echo $row->furni_id ?>/<?php echo $row->name ?>"><button>More Info</button></a>
            </div>

        <?php } ?>
    </div>
</div>