<?php
$users = $mysqli->query("SELECT id FROM users"); //Selected alle User
$rares = $mysqli->query("SELECT id FROM prices"); //Selected alle Rares
$news = $mysqli->query("SELECT id FROM news"); //Selected alle News
$user = $mysqli->query("SELECT id, username FROM users ORDER BY id DESC LIMIT 5");
$rare = $mysqli->query("SELECT id, name FROM prices ORDER BY id DESC LIMIT 5");
$new = $mysqli->query("SELECT id, title FROM news ORDER BY id DESC LIMIT 5");
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <div id="box" class="stats"> 
                Registrierted Users:
                <h3><?php echo $users->num_rows ?></h3>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div id="box" class="stats">
                Registered Rares:
                <h3><?php echo $rares->num_rows ?></h3>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div id="box" class="stats">
                Written News:
                <h3><?php echo $news->num_rows ?></h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <div id="box" class="ranked">
                <?php while ($row = $user->fetch_object()) { ?>
                    <div style="padding:10px;"><?php echo $row->username ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div id="box" class="ranked">
                <?php while ($row = $rare->fetch_object()) { ?>
                    <div style="padding:10px;"><?php echo $row->name ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div id="box" class="ranked">
                <?php while ($row = $new->fetch_object()) { ?>
                    <div style="padding:10px;"><?php echo $row->title ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>