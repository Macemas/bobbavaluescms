<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea',
    });</script>
<div class="container">
    <?php /*
    if ($GET[2] == 'write') {
        if (isset($_POST['submit'])) {
            if (
                    empty($_POST['title']) ||
                    empty($_POST['news_tiny']) ||
                    empty($_POST['url'])) {
                $error = true;
            } else {
                $mysqli->query("INSERT INTO news SET author = '" . $myrow->username . "', title = '" . $_POST['title'] . "', content = '" . $_POST['news_tiny'] . "', img = '" . $_POST['url'] . "', timestamp = '" . time() . "'");
            }
        }
        ?>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <form action="" method="POST">
                    <div style="color:black;">Title:</div>
                    <input name="title" type="text" style="width:100%;margin-bottom:10px;color:black;">
                    <div style="color:black;">Webpromo URL:</div>
                    <input name="url" type="text" style="width:100%;margin-bottom:10px;color:black;">
                    <textarea name="news_tiny"></textarea>
                    <button name="submit" style="margin-top:10px;">Save</button>
                </form>
            </div>
        </div>  
    <?php }  */ ?>

    <?php /*
    if ($GET[2] == 'all') {
        $news_query = $mysqli->query("SELECT id, title, img FROM news ORDER BY id DESC");
        ?>
        <div class="row">
            <?php while ($news = $news_query->fetch_object()) { ?>
                <div class="col-sm-12 col-md-3">
                    <div id="box" class="news_index" style="background:url(<?php echo $news->img ?>) 50% 50%;">
                        <div id="title"> <?php echo $news->title ?> </div>
                    </div>
                    <a href="<?php echo $_SITE['path'] ?>/acp/news/edit/<?php echo $news->id ?>/<?php echo $news->title ?>"><button>Editieren</button></a>
                </div>
            <?php } ?>
        </div>  
    <?php } */ ?>

    <?php
    if ($GET[2] == 'edit') { //page set to edit
        //$id = $GET[3]; News ID 
        $query = $mysqli->query("SELECT alert FROM cms_settings"); //get alert2 current data
        $row = $query->fetch_object();
        if (isset($_POST['del'])) { //post var del set 
            $mysqli->query("UPDATE cms_settings SET alert = '0'"); //update alert2 to 0
            header('Location: /acp'); //forward to acp
            exit;
        }
        if (isset($_POST['submit'])) { //check if form data is empty
            if (
                    empty($_POST['news_tiny'])) {
                $error = true;
            } else { //not empty update table
                $mysqli->query("UPDATE cms_settings SET alert = '" . $_POST['news_tiny'] . "'");
                header('Location:' . $_SITE['path'] . '/acp');
                exit;
            }
        }
        ?>

        <div class="row">
            <div class="col-sm-12 col-md-12">
                <form action="" method="POST">
                    <div style="color:black;">Editting <strong>Blue</strong> Alert</div>
					<textarea name="news_tiny"><?php echo $row->alert ?></textarea>
                    <button name="submit" style="margin-top:10px;">Save</button>
                    <button name="del" style="background:#c0392b;">Delete</button>
                </form>
            </div>
        </div>  
    <?php } ?>

</div>
