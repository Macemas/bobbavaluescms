<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea',
    });</script>
<div class="container">

    <?php
    if ($GET[2] == 'edit') { //page set to edit
        //$id = $GET[3]; News ID 
        $query = $mysqli->query("SELECT alert2 FROM cms_settings"); //get alert2 current data
        $row = $query->fetch_object();
        if (isset($_POST['del'])) { //post var del set 
            $mysqli->query("UPDATE cms_settings SET alert2 = '0'"); //update alert2 to 0
            header('Location: /acp'); //forward to acp
            exit;
        }
        if (isset($_POST['submit'])) { //check if form data is empty
            if (
                    empty($_POST['news_tiny'])) {
                $error = true;
            } else { //not empty update table
                $mysqli->query("UPDATE cms_settings SET alert2 = '" . $_POST['news_tiny'] . "'");
                header('Location:' . $_SITE['path'] . '/acp');
                exit;
            }
        }
        ?>

        <div class="row">
            <div class="col-sm-12 col-md-12">
                <form action="" method="POST">
                    <div style="color:black;">Editting <strong>Red</strong> Alert</div>
					<textarea name="news_tiny"><?php echo $row->alert2 ?></textarea>
                    <button name="submit" style="margin-top:10px;">Save</button>
                    <button name="del" style="background:#c0392b;">Delete</button>
                </form>
            </div>
        </div>  
    <?php } ?>

</div>
