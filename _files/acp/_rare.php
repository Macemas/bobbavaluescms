<?php
//Alle Kategorien
$kategorie_query = $mysqli->query("SELECT * FROM categories");

//Rare hinzufügen
if (isset($_POST['add'])) {
    $query = $mysqli->query("SELECT id FROM prices WHERE furni_id = '" . $_POST['habbo_furni_id'] . "'");
    if ($query->num_rows == 1) {
        $error = true;
    } else {
        $upload_folder = './public/img/rare/'; //Das Upload-Verzeichnis
        $filename = pathinfo($_POST['habbo_furni_id'], PATHINFO_FILENAME);
        $extension = strtolower(pathinfo($_FILES['datei']['name'], PATHINFO_EXTENSION));


//Überprüfung der Dateiendung
        $allowed_extensions = array('png', 'jpg', 'jpeg', 'gif');
        if (!in_array($extension, $allowed_extensions)) {
            die("Ungültige Dateiendung. Nur png, jpg, jpeg und gif-Dateien sind erlaubt");
        }

//Überprüfung der Dateigröße
        $max_size = 100000 * 1024;
        if ($_FILES['datei']['size'] > $max_size) {
            die("Bitte keine Dateien größer 500kb hochladen");
        }

//Überprüfung dass das Bild keine Fehler enthält
        if (function_exists('exif_imagetype')) { //Die exif_imagetype-Funktion erfordert die exif-Erweiterung auf dem Server
            $allowed_types = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
            $detected_type = exif_imagetype($_FILES['datei']['tmp_name']);
            if (!in_array($detected_type, $allowed_types)) {
                die("Nur der Upload von Bilddateien ist gestattet");
            }
        }

//Pfad zum Upload
        $new_path = $upload_folder . $_POST['habbo_furni_id'] . '.' . $extension;

//Neuer Dateiname falls die Datei bereits existiert
        if (file_exists($new_path)) { //Falls Datei existiert, hänge eine Zahl an den Dateinamen
            $id = 1;
            do {
                $new_path = $upload_folder . $_POST['habbo_furni_id'] . '_' . $id . '.' . $extension;
                $id++;
            } while (file_exists($new_path));
        }

//Alles okay, verschiebe Datei an neuen Pfad
        move_uploaded_file($_FILES['datei']['tmp_name'], $new_path);
        $mysqli->query("INSERT INTO prices SET furni_id = '" . $_POST['habbo_furni_id'] . "', name = '" . $_POST['name'] . "', price = '" . $_POST['price'] . "', category = '" . $_POST['category'] . "', last_timestamp = '" . time() . "', last_author = '" . $myrow->username . "'");
    }
}
?>
<div class="container">

    <?php if ($GET[2] == 'add') { ?>
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <div id="box" class="rare_add">
                    <form action="" method="POST" enctype="multipart/form-data">
                        <input name="habbo_furni_id" id="habbo_furni_id" value="0">
                        Name:
                        <input name="name" onkeyup="SearchByName(this.value)">
                        Category:
                        <select name="category">
                            <?php while ($row = $kategorie_query->fetch_object()) { ?>
                                <option value="<?php echo $row->id ?>"><?php echo $row->name ?></option>
                            <?php } ?>
                        </select>
                        Value:
                        <input name="price">
                        <input type="file" name="datei">
                        <button name="add">Add</button>
                    </form>
                </div>
            </div>
            <div class="col-sm-12 col-md-8">
                <div id="box" class="rare_add">
                    <div class="load"></div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php
    if ($GET[2] == 'edit') {
        $query = $mysqli->query("SELECT * FROM prices WHERE furni_id = '" . $GET[3] . "'");
        $row = $query->fetch_object();
        //Möbel editieren
        if (isset($_POST['edit'])) {
            $mysqli->query("UPDATE prices SET last_timestamp = '" . time() . "', last_author = '" . $myrow->username . "', last_price = '" . $_POST['last_price'] . "', name = '" . $_POST['name'] . "', price = '" . $_POST['price'] . "'  WHERE furni_id = '" . $GET[3] . "'");
        }
//Möbel löschen
        if (isset($_POST['del'])) {
            $mysqli->query("DELETE FROM prices WHERE furni_id = '" . $GET[3] . "'");
            exit;
        }
        ?>
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div id="box" class="rare_add">
                    <form action="" method="POST">
                        <input type="hidden" name="id" value="<?php echo $row->id ?>">
                        <input type="hidden" name="last_price" value="<?php echo $row->price ?>">
                        Name:
                        <input name="name" value="<?php echo $row->name ?>">
                        Value:
                        <input name="price" value="<?php echo $row->price ?>">
                        <button name="edit">Add</button>  
                        <button type="submit" style="background:#c0392b;" name="del">Delete</button>
                    </form>
                </div>
            </div>
        </div>

    <?php } ?>


</div>