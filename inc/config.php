<?php

/*
  _    _       _     _           _____          _
  | |  | |     | |   | |         |  __ \        (_)
  | |__| | __ _| |__ | |__   ___ | |__) | __ ___ _ ___
  |  __  |/ _` | '_ \| '_ \ / _ \|  ___/ '__/ _ \ / __|
  | |  | | (_| | |_) | |_) | (_) | |   | | |  __/ \__ \
  |_|  |_|\__,_|_.__/|_.__/ \___/|_|   |_|  \___|_|___/
  @BY: MUSTI
  @BUILD: 1
  @MAIL: mustifreak@retrotown.ws
 */
//Datenbank Zugangsdaten
$_DB = array(
    "host" => "127.0.0.1",
    "user" => "user",
    "passwort" => "user",
    "datenbank" => "rares",
    "port" => "3306"
);

//Definierung der $mysqli Variable
$mysqli = new mysqli($_DB['host'], $_DB['user'], $_DB['passwort'], $_DB['datenbank'], $_DB['port']);

//Wenn keine Verbindung aufgebaut werden kann wird das Skript beendet
if ($mysqli->connect_error) {
    exit;
}

//CMS Settings
$config = $mysqli->query("SELECT * FROM cms_settings");
$global = $config->fetch_object();
$_SITE = array(
    "name" => $global->name,
    "api" => $global->api,
    "path" => $global->path,
    "hotel" => $global->hotel,
    "alert" => $global->alert,
    "alert2" => $global->alert2,
        )
?>