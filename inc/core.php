<?php

/*
  _    _       _     _           _____          _
  | |  | |     | |   | |         |  __ \        (_)
  | |__| | __ _| |__ | |__   ___ | |__) | __ ___ _ ___
  |  __  |/ _` | '_ \| '_ \ / _ \|  ___/ '__/ _ \ / __|
  | |  | | (_| | |_) | |_) | (_) | |   | | |  __/ \__ \
  |_|  |_|\__,_|_.__/|_.__/ \___/|_|   |_|  \___|_|___/
  @BY: MUSTI
  @BUILD: 1
  @MAIL: musti@retrotown.ws
 */
error_reporting(0); //Blendet PHP Error aus
session_start(); //Startet eine Session


// Filterung gegen HTML UND JS Tags
function protect($string, $output = false) {
    $string = str_replace("'", "", $string);
    $string = str_replace("alert(", "", $string);
    $string = str_replace("prompt(", "", $string);
    $string = str_replace("/*", "", $string);
    $string = str_replace("document.cookie", "", $string);
    $string = str_replace('document["cookie"]', '', $string);
    $string = str_replace('window["alert"', '', $string);

    $string = strip_tags($string);

    return $string;
}

//XSS Schutz
if ($_POST) {
    foreach ($_POST as $key => $val) {
        if ($key == 'news_tiny') {
            
        } else {
            $_POST[$key] = $mysqli->real_escape_string(protect($val));
        }
    }
}
if ($_GET) {
    foreach ($_GET as $key => $val) {
        $_GET[$key] = $mysqli->real_escape_string(protect($val));
    }
}

//Browser
function get_browser_name($user_agent) {
    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/'))
        return 'Opera';
    elseif (strpos($user_agent, 'Edge'))
        return 'Edge';
    elseif (strpos($user_agent, 'Chrome'))
        return 'Chrome';
    elseif (strpos($user_agent, 'Safari'))
        return 'Safari';
    elseif (strpos($user_agent, 'Firefox'))
        return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7'))
        return 'Internet Explorer';

    return 'Other';
}

//User IP auslesen
if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_FORWARDED_FOR'];
}

//Passwort verschlüsslung
function pwhash($string) {
    $string = protect($string);
    $verschlusselung = 'XX8402u7NF.TL.85929!FASDJ4FU!A\\//adgkl4hsfkasdfhas,l54jk';

    $pw = md5(md5(md5($string) . $verschlusselung));
    return $pw;
}

if (isset($_SESSION['username'])) {
    $username = protect($_SESSION['username']);
    $password = protect($_SESSION['password']);

    $check = $mysqli->query("SELECT * FROM users WHERE username = '" . $username . "' AND password = '" . $password . "' ");
    if ($check->num_rows > 0) {
        $myrow = $check->fetch_object();
    } else {
        if (Userlogout()) {
            header('Location:' . $_SITE['path'] . '/index');
            exit;
        }
    }
} else {
    $myrow->username = 'Guest';
    $myrow->rank = 0;
}

function Userlogout() {
    session_destroy;

    return true;
}

?>
