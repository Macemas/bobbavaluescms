<?php
if (isset($_POST['seach'])) {
    header('Location: ' . $_SITE['path'] . '/search/' . $_POST['search_begriff']);
}

date_default_timezone_set('Europe/Berlin');
header('Content-Type: text/html; charset=UTF-8');

//Register
if (isset($_POST['register'])) {
	$checkuser = $mysqli->query("SELECT username FROM users WHERE username = '".$_POST['username']."'");
    $username = $_POST['username'];
    $password = $_POST['password'];
    $password_wdh = $_POST['password_wdh'];
    $string = $_POST['username'];
    $mail = $_POST['email'];

    if
    (
            empty($username) ||
            empty($password) ||
            empty($password_wdh) ||
            empty($mail)) {

        $error = true;
        $msg = 'Please fill out everything!';
    }

    if ($password !== $password_wdh) {
        $error = true;
        $msg = 'Passwords do not match.';
    }

    if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        $error = true;
        $msg = 'Please enter a valid email address.';
    }
	
	if($checkuser->num_rows == 1 ) {
		 $error = true;
         $msg = 'The username you gave is already in use.';
	}

    if ($error == false) {
        $query = $mysqli->query("INSERT INTO users SET username = '" . $username . "', password = '" . pwhash($password) . "', email = '" . $mail . "', rank = '1', desc_text = 'User'");
        $_SESSION['username'] = $username;
        $_SESSION['password'] = pwhash($password);
        header('Location:' . $_SITE['path'] . '/index');
        exit;
    }
}

//Einloggen
if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = pwhash($_POST['password']);
    $query = $mysqli->query("SELECT username, password FROM users WHERE username = '" . $username . "' AND password = '" . $password . "'");
    if ($query->num_rows == 1) {
        $_SESSION['username'] = $username;
        $_SESSION['password'] = $password;
        header('Location:' . $_SITE['path'] . '/index');
        exit;
    } else {
        header('Location:' . $_SITE['path'] . '/index');
        exit;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $_SITE['name'] ?> - Your Values page</title>
        <meta name="keywords" content="bobbavalues.com, retro, habbo, fansite, values, values page, rares, classichabbo">
        <meta name="description" content="BOBBAValues.com - Your values page for classichabbo.com">
        <meta name="publisher" content="Copyright">
        <meta name="Content-Language" content="en">
        <link href="<?php echo $_SITE['path'] ?>/public/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="<?php echo $_SITE['path'] ?>/public/js/bootstrap.min.js"></script>
        <script src="<?php echo $_SITE['path'] ?>/public/js/script.js"></script>
        <?php include 'style.php'; ?>
    </head>
    <body>
        <nav class="nav navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><?php echo $_SITE['name'] ?></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo $_SITE['path'] ?>/">Homepage <span class="sr-only">(current)</span></a></li>
                        <li><a href="<?php echo $_SITE['path'] ?>/category/all">All categories</a></li>
                        <li><a href="<?php echo $_SITE['hotel'] ?>">ClassicHabbo</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php if (!isset($_SESSION['username'])) { ?>
                            <li><a style="cursor:pointer;" onclick="login()">Log in</a></li>
                            <li><a style="cursor:pointer;" onclick="register()">Register</a></li>
                        <?php } else { ?>
                           <?php if($myrow->rank > 1) { echo '<li><a href="'.$_SITE['path'].'/acp">Backend</a></li>';  } ?>
                            <li><a href=""><?php echo $myrow->username ?></a></li>
                            <li><a href="<?php echo $_SITE['path'] ?>/logout">Log out</a></li>
                        <?php } ?>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
		
		
        <header>
            <div class="container">
                <a href="<?php echo $_SITE['path'] ?>"><img id="logo" src="<?php echo $_SITE['path'] ?>/public/img/logo.png"></a>
            </div>
			            <div class="clouds">
                
            </div>
        </header>


        <div class="container">
            <div class="row">
                
                <div class="col-sm-12 col-md-12">
                    
                    <?php if(!$_SITE['alert2'] == '0'){ echo '<div id="sitealertred"><b>ALERT :</b> '.$_SITE['alert2'].'</div>'; } ?>
                    <?php if(!$_SITE['alert'] == '0') { echo '<div id="sitealert"><b>ALERT :</b> '.$_SITE['alert'].'</div>'; } ?>
                    
                </div>
            </div>
            <div class="row">
                <form action="" method="POST">
                    <div class="col-sm-12 col-md-12">
                        <input id="search" name="search_begriff" type="text" placeholder="Search Rare">
                        <input type="submit" id="search_submit" name="seach" value="Search">
                    </div>
                </form>
            </div>
        </div>
        <div class="container">
            <div id="overlay"> 
                <div id="login">
                    <button id="close" onclick="close_login()"></button>
                    <h4> Log in </h4>
                    <form action="" method="POST">
                        <input type="text" class="username" name="username" placeholder="Username">
                        <input type="password" class="password" name="password" placeholder="Password">
                        <button name="login">Log in</button>
                    </form>
                    <button onclick="login_register()" id="register">Register</button>
                </div>
            </div>
        </div>
        <div class="container">
            <div id="overlay_register"> 
                <div id="login" style="height:400px;">
                    <button id="close" onclick="close_register()"></button>
                    <h4> Register </h4>
                    <form action="" method="POST">
                        <input type="text" class="username" name="username" placeholder="Username">
                        <input type="password" class="password" name="password" placeholder="Password">
                        <input type="password" class="password" name="password_wdh" placeholder="Repeat password">
                        <input type="mail" class="mail" name="email" placeholder="Email">
                        <button name="register">Register</button>
                    </form>
                    <button onclick="register_login()" id="register">Log in</button>
                </div>
            </div>
        </div>