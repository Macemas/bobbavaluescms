<?php
date_default_timezone_set('Europe/Berlin');
header('Content-Type: text/html; charset=UTF-8');
if ($myrow->rank < 2) {
    header('Location: /index');
    exit;
} else {
    ?>
    <!DOCTYPE html>
    <html>
        <head>
            <title><?php echo $_SITE['name'] ?> - Backend</title>
            <meta name="keywords" content="habbopreis.xyz, musti, retro, habbo, fanseite, habbopreis, preisliste, rares, madox">
            <meta name="description" content="HabboPreis - Deine offizielle Preisliste zum Habbo Hotel">
            <meta name="publisher" content="Musti">
            <meta name="Content-Language" content="de">
            <link href="<?php echo $_SITE['path'] ?>/public/css/bootstrap.min.css" rel="stylesheet">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script src="<?php echo $_SITE['path'] ?>/public/js/bootstrap.min.js"></script>
            <script src="<?php echo $_SITE['path'] ?>/public/js/script.js"></script>
            <?php include 'style.php'; ?>
        </head>
        <body>
            <nav class="nav navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><?php echo $_SITE['name'] ?> ACP</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo $_SITE['path'] ?>/acp">Homepage <span class="sr-only">(current)</span></a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">News <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo $_SITE['path'] ?>/acp/news/write">Write news</a></li>
                                    <li><a href="<?php echo $_SITE['path'] ?>/acp/news/all">All news</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Rare <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo $_SITE['path'] ?>/acp/rare/add">Add Rare</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Red alert <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo $_SITE['path'] ?>/acp/redalert/edit">Edit Red Alert</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Blue alert <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo $_SITE['path'] ?>/acp/bluealert/edit">Edit Blue Alert</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="<?php echo $_SITE['path'] ?>"><?php echo $myrow->username ?></a></li>
                            <li><a href="<?php echo $_SITE['path'] ?>/logout">Log out</a></li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

        <?php } ?>
