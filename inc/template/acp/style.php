<style>
    @import url('https://fonts.googleapis.com/css?family=Open+Sans');
    a {
        color:white;
    }
    a:hover {
        color:white;
    }
    a:visited {
        color:white;
    }
    body {
        background:#ECF0F1;
        color:white;
    }
    .navbar-default .navbar-nav>li>a {
        color:white;
    }
    .navbar-default .navbar-nav>li>a:hover {
        color:white;
    }
    .navbar-default .navbar-nav>li>a:focus {
        color:white;
    }
    .navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover {
        color: white;
        background-color: #2C3E50;
    }
    .navbar-default .navbar-brand {
        color: white;
    }  
    .navbar-default .navbar-brand:focus {
        color:white;
    }
    .navbar-default .navbar-brand:hover {
        color: white;
    }
    .nav {
        border-radius:0px;
        border:0;
        background:#2c3e50;
        color:white;
    }
    #tab {
        float: left;
        height: 50px;
        line-height: 50px;
        padding-left: 20px;
        padding-right: 20px;
        text-align: center;
        cursor: pointer;
    }
    #tab:hover {
        background:#34495e;
    }
    header {
        background:#43607C;
        height:200px;
        width:100%;
        margin-bottom:10px;
        margin-top:-20px;
    }
    #sitealert {
        background:#2c3e50;
        padding:10px;
        padding-left:10px;
        padding-right:10px;
        margin-bottom:10px;
    }
    #logo {
        margin-top:40px;
    }
    #logo:hover {
        opacity:0.6;
    }
    #box {
        background:white;   
        height:300px;
        color:black;
    }
    #box.news_index {
        height:170px;
        border:2px solid rgba(0,0,0,0.1);
    }
    #box.news_index #title {
        background:rgba(0,0,0,0.5);
        width:100%;
        height:50px;
        line-height:50px;
        text-align:center;
        color:white;
        border-bottom:2px solid rgba(0,0,0,0.5);
    }
    #box.rare_box {
        height:200px;
        background:url('<?php echo $_SITE['path'] ?>/public/img/itembg.png')50% 50%;
    }
    #box.rare_box #title {
        background:rgba(0,0,0,0.6);
        height:40px;
        line-height:40px;
        text-align:center;
        border:1px solid rgba(0,0,0,0.4);
        border-bottom:2px solid rgba(0,0,0,0.4);
        color:white;
    }
    .preis {
        background:url(<?php echo $_SITE['path'] ?>/public/img/credits.gif) 10px 50% no-repeat;
        background-color:#98721A; 
        border:2px solid #D19E25;
        border-radius:4px;
        position:absolute;
        width:auto;
        padding:10px;
        padding-left:35px;
        color:white;
        height:35px;
        line-height:10px;
        margin-top:45px;
        margin-left:10px;
    }
    button {
        height:50px;
        background:#34495e;
        border:0;
        color:white;
        width:100%;
        margin-bottom:10px;
    }
    #title {
        float: left;
        width:100%;
        height: 30px;
        color: #2c3e50;
        margin-bottom: 15px;
        line-height: 30px;
        font-size: 15px;
        font-weight: bold;
        border-bottom: 2px solid #2c3e50;
    }
    #box #inner {
        height:230px;
    }
    #box.leiste {
        height:30px;
        margin-left:-30px;
        padding-left:10px;
        line-height:30px;
    }
    #box.leiste img {
        margin-right:10px;   
    }
    #box.leiste.two {
        background:#ECF0F1;
    }
    #search {
        float: left;
        width:950px;
        height: 34px;
        padding-left: 35px;
        border: 1px solid rgba(0, 0, 0, 0.2);
        background: url(<?php echo $_SITE['path'] ?>/public/img/search.gif) 10px 50% no-repeat;
        background-color: #FFF;
        margin-bottom:10px;
        color:black;
    }
    #search_submit {
        float: left;
        width: 190px;
        height: 34px;
        color: #FFF;
        background: #2c3e50;
        border: 1px solid rgba(0, 0, 0, 0.2);
        cursor: pointer;
        -webkit-appearance: none;
    }
    #box.news_list {
        height:auto;
        padding:20px;
    }
    #box.news_list .news_select {  
        height:50px;
        width:100%;
        line-height:50px;
        padding-left:10px;
    }
    #box.news_list .news_select:hover {
        background:#F3F3F3; 
    }
    #box.news_list .news_select.select {
        font-weight:bold;   
    }
    #box.news_body {
        padding:10px;   
    }
    #box.news_body h4 {
        padding:10px;
        border-bottom: 1px solid rgba(0, 0, 0, 0.08);
    }
    #box.news_author {
        margin-top:10px;
        margin-bottom:10px;
        height:50px;
        padding:10px;
        line-height:30px;
    }
    #box.partner {
        height: 120px;
        text-align:center;
        margin-bottom:10px;
        padding:10px;
        color:white;
        background:url(<?php echo $_SITE['path'] ?>/public/img/Bg_hotcool_summer.gif) 30% 50%;
    }
    footer {
        background:#2C3E50;
        height:200px;
        width:100%;
        height:50px;
        padding-left:15px;
        padding-right:15px;
        line-height:50px;
        color:white;
        margin-top:10px;
    }
    #box input {
        height:50px;
        width:100%;
        border:1px solid rgba(0,0,0,0.2);
        margin-bottom:10px;
        padding-left:35px;
    }
    #overlay {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .6);
        z-index: 1000000000000;
        display:none;
    }
    #overlay_register {
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, .6);
        z-index: 1000000000000;
        display:none;
    }

    #login {
        width:350px;
        height:300px; 
        background: rgba(255, 255, 255, 1);
        padding:10px;
        margin: auto;
        margin-top:100px;
        z-index:99999999999999999999999;
        -webkit-box-shadow: 10px 10px 15px -6px rgba(0,0,0,0.5);
        -moz-box-shadow: 10px 10px 15px -6px rgba(0,0,0,0.5);
        box-shadow: 10px 10px 15px -6px rgba(0,0,0,0.5);
        border-radius:0px;
        color:black;
    }
    #login h4 {
        font-size:19px;   
        margin-top:0px;
    }
    #login input {
        height:50px;
        width:calc(345px - 15px);
        margin-bottom:10px;
        border:1px solid rgba(0,0,0,0.2);
        padding-left:35px;
        float:left;
        padding-right:10px;
    }
    #login input.username {
        background: url('<?php echo $_SITE['url'] ?>/public/img/icon_author_alt.gif') 10px 50% no-repeat;  
    }
    #login input.password {
        background: url('<?php echo $_SITE['url'] ?>/public/img/key.gif') 10px 50% no-repeat;  
    }
    #login input.mail {
        background: url('<?php echo $_SITE['url'] ?>/public/img/mail.gif') 10px 50% no-repeat;  
    }
    #login button {
        height:50px;   
        width:330px;
        float:left;
        background:#7CB342;
        border:0;
        color:white;
        cursor:pointer;
        margin-bottom:10px;
    }
    #login button:hover {
        background:#689F38;
    }
    #login button:active {
        background:#558B2F;
    }
    #login button:focus {
        border:0;
    }
    #login #register {
        background:#D32F2F;   
    }
    #login #register:hover {
        background:#C62828;
    }
    #login #register:active {
        background:#B71C1C;
    }
    #login #close {
        width:19px;   
        height:20px;
        background:url('<?php echo $_SITE['url'] ?>/public/img/close_normal.png') no-repeat;
        float:right;
        cursor:pointer;
        border:0;

    }
    #login #close:hover {
        background:url('<?php echo $_SITE['url'] ?>/public/img/close_hover.png') no-repeat;
    }
    #login #close:active {
        background:url('<?php echo $_SITE['url'] ?>/public/img/close_active.png') no-repeat;
    }
    textarea {
        width:100%;
        height:100px;
        color:black;
        padding:10px;
        border:1px solid rgba(0,0,0,0.2);
    }
    #box.comment_user {
        padding:20px;
        padding-left:65px;
        height:auto;
        margin-bottom:10px;
    }
    #box.stats {
        height:100px;   
        padding:20px;
        margin-bottom:10px;
    }
    #box.stats h3 {
        margin-top:5px;   
    }
    #box.ranked {
        height:auto;
        padding:10px;
        text-align:center;
    }
    #box.rare_add {
        height:auto;
        padding:10px;
    }
    .leiste {
        background:#303F9F;
        padding:10px;
        width:100%;
        height:auto;
        margin-bottom:10px;
        color:white;
    }
    .success {
        background:#03A9F4;
    }
    select {
     width:100%;
     height:50px;
     margin-bottom:10px;
    }
</style>